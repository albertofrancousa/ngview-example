/// <reference path="typings/index.d.ts"/>

var http = require('http');
var morgan = require('morgan')
var path = require('path');
var express = require('express');
var app = express();

app.set('port', process.env.PORT || 3000);
app.use(morgan('tiny'));
app.use(express.static(path.join(__dirname, 'public')));

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});