var app = angular.module('app', ['ngRoute', 'ngAnimate']);

app.config(['$routeProvider', '$locationProvider',
  function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/Book/:bookId', {
        templateUrl: 'book.html',
        controller: 'BookCtrl',
        controllerAs: 'book'
      })
      .when('/Book/:bookId/ch/:chapterId', {
        templateUrl: 'chapter.html',
        controller: 'ChapterCtrl',
        controllerAs: 'chapter'
      });

    // $locationProvider.html5Mode(true);
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: true
    });
  }
]);

app.controller('MainCtrl', ['$route', '$routeParams', '$location',
  function MainCtrl($route, $routeParams, $location) {
    this.$route = $route;
    this.$location = $location;
    this.$routeParams = $routeParams;
  }
]);

app.controller('BookCtrl', ['$routeParams', function BookCtrl($routeParams) {
  this.name = 'BookCtrl';
  this.params = $routeParams;
}]);

app.controller('ChapterCtrl', ['$routeParams', function ChapterCtrl($routeParams) {
  this.name = 'ChapterCtrl';
  this.params = $routeParams;
}]);